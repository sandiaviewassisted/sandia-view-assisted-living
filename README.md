We treat our residents as members of our family. At Sandia View Assisted Living, patient comfort, safety and proper nutrition is paramount and it is why we believe we are the best assisted living facility in Albuquerque and Rio Rancho. Call +1(505) 681-3716 for more information!

Address: 12501 Royal Point Ct NE, Albuquerque, NM 87111, USA

Phone: 505-681-3716

Website: http://www.sandiaviewassistedliving.org/our-homes/royal-point-home/
